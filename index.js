// console.log("Hello")

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
    function register(userName){
  
         
         	    let x = registeredUsers.map(element => element.toLowerCase());
         	    if(x.indexOf(userName.toLowerCase())>=0){
         	    	alert("Registration failed. Username already exists!");
         	    
         	     } else {

         	    	alert("Thank you for registering!")
         	    	 registeredUsers.push(userName);
         	       
         	    	
         	    }
         	    
               }
                //registeredUsers("Kenneth Louie")
    	// let registered = register(prompt("Enter the fullname: "));
    	// console.log(registeredUsers);

 
/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.
*/      

    function addFriends(addName){

               let regusers = registeredUsers.map(element => element.toLowerCase());
               if(regusers.indexOf(addName.toLowerCase())>=0){
                     alert("You have added "+addName+" as a friend!");
                     friendsList.push(addName);
               } else{
               	alert("User not found!!.");
               }

                 
	           }
	           // addFriends(prompt("enter full name from registeredUsers: "));

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

// */
		     function myFriendList(myList){
		        if(friendsList.length == 0){
		        	alert("You currently have 0 friends. Add one first.")
		        } else if(friendsList.length > 0){
		        	for(let lists = 0; lists < friendsList.length; lists++){
		        		console.log(friendsList[lists]);
		        	}
		        }
		     }
		    //  showListOfF =  myFriendList(friendsList);
		    // console.log(showListOfF);
		    

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
    function numberOfFriends(amountFriend){
    	if(friendsList.length == 0){
    		alert("You currently have 0 friends. Add one first.")
    	} else if(friendsList.length > 0){
    		alert("You currently have "+friendsList.length+" friends.")
    		 friendsList.length;
    	
    	}

    }
     	// let showNumberofFriends = numberOfFriends(friendsList);
   			// 	console.log(showNumberofFriends);
    

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/    
       function deleteFriend(){
         
         if(friendsList.length == 0){
         	alert("You currently have 0 friends. Add one first.");
         } else {
         	let del = friendsList.pop();
         }
       }
       // deleteFriend(friendsList);
       // console.log(friendsList);


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/     

       //PLEASE UNCOMMENT THE ALGO BELOW THE ELSE STATEMENT THANKYOU!

    let startIndet;
    let delCount;
    let name;

    function spliceFunc(currentFriendlist){
              if(friendsList.length == 0){
              	alert("You currently have 0 friends. Add one first.");
              } else {
              	//let spliceThat =friendsList.splice(startIndet = prompt("Enter your Starting Index: "),
              		//delCount = prompt("Enter your delete count"),
              		//name = prompt("Do u want to add name? if no leave it blank"));
              	//return spliceThat;
              }
    }
   // let showFl = spliceFunc(friendsList);
   //    console.log(friendsList);
